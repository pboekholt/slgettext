#---------------------------------------------------------------------------
# Installation Directories
#---------------------------------------------------------------------------
prefix = /usr/local
exec_prefix = ${prefix}
MODULE_INSTALL_DIR = ${exec_prefix}/lib/slang/v2/modules
SL_FILES_INSTALL_DIR = ${prefix}/share/slsh/local-packages
HLP_FILES_INSTALL_DIR = $(SL_FILES_INSTALL_DIR)/help

#---------------------------------------------------------------------------
# DESTDIR is designed to facilitate making packages.  Normally it is empty
#---------------------------------------------------------------------------
DESTDIR =
DEST_MODULE_INSTALL_DIR = $(DESTDIR)$(MODULE_INSTALL_DIR)
DEST_SL_FILES_INSTALL_DIR = $(DESTDIR)$(SL_FILES_INSTALL_DIR)
DEST_HLP_FILES_INSTALL_DIR = $(DESTDIR)$(HLP_FILES_INSTALL_DIR)

#---------------------------------------------------------------------------
# Location of the S-Lang library and its include file
#---------------------------------------------------------------------------
SLANG_INC	= -I${prefix}/include
SLANG_LIB	= -L${prefix}/lib -lslang

#---------------------------------------------------------------------------
# C Compiler to create a shared library
#---------------------------------------------------------------------------
CC_SHARED 	= $(CC) $(CFLAGS) -shared -Wall -fPIC

#---------------------------------------------------------------------------
# Misc Programs required for installation
#---------------------------------------------------------------------------
INSTALL		= /usr/bin/install -m 644
MKINSDIR        = /usr/bin/install -d
#---------------------------------------------------------------------------
# You shouldn't need to edit anything below this line
#---------------------------------------------------------------------------

LIBS = $(SLANG_LIB) -lm
INCS = $(SLANG_INC)

all: gettext-module.so

gettext-module.so: gettext-module.c
	$(CC_SHARED) $(INCS) gettext-module.c -o gettext-module.so $(LIBS)

gettext.o: gettext-module.c
	gcc $(CFLAGS) $(INCS) -O2 -c -g gettext-module.c -o gettext.o

clean:
	rm -f gettext-module.so *.o

#---------------------------------------------------------------------------
# Installation Rules
#---------------------------------------------------------------------------
install_directories:
	$(MKINSDIR) $(DEST_MODULE_INSTALL_DIR)
	$(MKINSDIR) $(DEST_SL_FILES_INSTALL_DIR)
	$(MKINSDIR) $(DEST_HLP_FILES_INSTALL_DIR)

install_modules:
	$(INSTALL) gettext-module.so $(DEST_MODULE_INSTALL_DIR)
install_slfiles:
	$(INSTALL) gettext.sl $(DEST_SL_FILES_INSTALL_DIR)
install_hlpfiles:
	$(INSTALL) tm/gettext.hlp $(DEST_HLP_FILES_INSTALL_DIR)

install: all install_directories install_modules install_slfiles install_hlpfiles

