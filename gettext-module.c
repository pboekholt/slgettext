/*{{{ slirp boilerplate */

#include <slang.h>
#include <locale.h>
#include <libintl.h>

static void Slirp_usage_err(const char *usage_str)
{
   char msg[257];
   int npop = SLstack_depth();
   if (npop > SLang_Num_Function_Args)
	npop = SLang_Num_Function_Args;
   SLdo_pop_n(npop);
   snprintf(msg,248,"Usage: %s",usage_str);
   SLang_verror(SL_USAGE_ERROR, msg);
}
/*}}}*/
/*{{{ pop string or NULL */
/* adapted from slirps pop_obj_or_null() */
static int pop_string_or_null(char **obj)
{
   if (SLANG_NULL_TYPE == SLang_peek_at_stack ()) { 
	*obj = NULL;
	return SLang_pop_null ();
   }
   return SLang_pop_slstring ((char**)obj);
}

/*}}}*/
/*{{{ gettext wrappers */

static void sldcngettext (void)
{
   char* result;
   char* arg1;
   char* arg2;
   char* arg3;
   unsigned long arg4;
   int arg5;

   if (SLang_Num_Function_Args != 5 ||
	SLang_pop_integer(&arg5) == -1 ||
	SLang_pop_ulong(&arg4) == -1 ||
	SLang_pop_slstring(&arg3) == -1 ||
	SLang_pop_slstring(&arg2) == -1 ||
	SLang_pop_slstring(&arg1) == -1 )
	{Slirp_usage_err( "string = dcngettext(string domainname, string msgid, \
string msgid2, ulong, int category)"); return;}

   result = dcngettext(arg1,
			arg2,
			arg3,
			arg4,
			arg5);
   (void) SLang_push_string ( result);
}

static void slgettext (void)
{
   char* result;
   char* arg1;

   if (SLang_Num_Function_Args != 1 ||
	SLang_pop_slstring(&arg1) == -1 )
	{Slirp_usage_err( "string = gettext(string msgid)"); return;}

   result = gettext(arg1);
   (void) SLang_push_string ( result);
}

static void sldngettext (void)
{
   char* result;
   char* arg1;
   char* arg2;
   char* arg3;
   unsigned long arg4;

   if (SLang_Num_Function_Args != 4 ||
	SLang_pop_ulong(&arg4) == -1 ||
	SLang_pop_slstring(&arg3) == -1 ||
	SLang_pop_slstring(&arg2) == -1 ||
	SLang_pop_slstring(&arg1) == -1 )
	{Slirp_usage_err( "string = dngettext(string domainname, string msgid, \
string msgid2, ulong n)"); return;}

   result = dngettext(arg1,
			arg2,
			arg3,
			arg4);
   (void) SLang_push_string ( result);
}

static void slbindtextdomain (void)
{
   char* result;
   char* arg1;
   char* arg2;

   if (SLang_Num_Function_Args != 2 ||
	pop_string_or_null(&arg2) == -1 ||
	SLang_pop_slstring(&arg1) == -1 )
	{Slirp_usage_err( "string = bindtextdomain(string,string or NULL)"); return;}

   result = bindtextdomain(arg1,
			arg2);
   (void) SLang_push_string ( result);
}

static void slbind_textdomain_codeset (void)
{
   char* result;
   char* arg1;
   char* arg2;

   if (SLang_Num_Function_Args != 2 ||
	pop_string_or_null(&arg2) == -1 ||
	SLang_pop_slstring(&arg1) == -1 )
	{Slirp_usage_err( "string = bind_textdomain_codeset(string domain, string or NULL codeset)"); return;}

   result = bind_textdomain_codeset(arg1,
			arg2);
   (void) SLang_push_string ( result);
}

static void sldgettext (void)
{
   char* result;
   char* arg1;
   char* arg2;

   if (SLang_Num_Function_Args != 2 ||
	SLang_pop_slstring(&arg2) == -1 ||
	SLang_pop_slstring(&arg1) == -1 )
	{Slirp_usage_err( "string = dgettext(string domain, string msgid)"); return;}

   result = dgettext(arg1,
			arg2);
   (void) SLang_push_string ( result);
}

static void sltextdomain (void)
{
   char* result;
   char* arg1;

   if (SLang_Num_Function_Args != 1 ||
	pop_string_or_null(&arg1) == -1 )
	{Slirp_usage_err( "string = textdomain(string or NULL domain)"); return;}

   result = textdomain(arg1);
   (void) SLang_push_string ( result);
}

static void slngettext (void)
{
   char* result;
   char* arg1;
   char* arg2;
   unsigned long int arg3;

   if (SLang_Num_Function_Args != 3 ||
	SLang_pop_ulong(&arg3) == -1 ||
	SLang_pop_slstring(&arg2) == -1 ||
	SLang_pop_slstring(&arg1) == -1 )
	{Slirp_usage_err( "string = ngettext(string msgid, string msgid2, ulong n)"); return;}

   result = ngettext(arg1,
			arg2,
			arg3);
   (void) SLang_push_string ( result);
}

static void sldcgettext (void)
{
   char* result;
   char* arg1;
   char* arg2;
   int arg3;

   if (SLang_Num_Function_Args != 3 ||
	SLang_pop_integer(&arg3) == -1 ||
	SLang_pop_slstring(&arg2) == -1 ||
	SLang_pop_slstring(&arg1) == -1 )
	{Slirp_usage_err( "string = dcgettext(string domain, string msgid, int category)"); return;}

   result = dcgettext(arg1,
			arg2,
			arg3);
   (void) SLang_push_string ( result);
}

/*}}}*/
/*{{{ setlocale */

static void slsetlocale(const int *l, const char *lc)
{
   char* result;
   /*
    * Setting LC_NUMERIC to something other than "C" breaks S-Lang's
    * floating point support.
    * For backward compatibility the LC_ALL and LC_NUMERIC constants
    * are still available, however LC_ALL sets LC_MESSAGES instead
    * and LC_NUMERIC does nothing.
    */
   if (*l == LC_ALL)
     {
	result = setlocale(LC_MESSAGES, lc);
     }
   else if (*l == LC_NUMERIC)
     {
	result = "C";
     }
   else
     {
	result = setlocale (*l, lc);
     }
   (void) SLang_push_string ( result ? result : "");
}

/*}}}*/
/*{{{ unary string op */

/* this defines a unary minus for string types. */
static int string_unary_result (int op, SLtype a, SLtype *b)
{
   if (op != SLANG_CHS)
     return 0;
   *b = SLANG_STRING_TYPE;
   return 1;
}

static int string_unary (int op,
			  SLtype a_type, VOID_STAR ap, unsigned int na,
			  VOID_STAR bp)
{
   char **a, **b;
   a = (char **)ap;
   b = (char **)bp;
   *b = SLang_create_slstring(gettext(*a));
   return 1;
}

/*}}}*/
/*{{{ function table */

static SLang_Intrin_Fun_Type gettext_Funcs [] =
{
   MAKE_INTRINSIC_0((char*)"dcngettext",sldcngettext, SLANG_VOID_TYPE),
   MAKE_INTRINSIC_0((char*)"gettext",slgettext, SLANG_VOID_TYPE),
   MAKE_INTRINSIC_0((char*)"dngettext",sldngettext, SLANG_VOID_TYPE),
   MAKE_INTRINSIC_0((char*)"bindtextdomain",slbindtextdomain, SLANG_VOID_TYPE),
   MAKE_INTRINSIC_0((char*)"bind_textdomain_codeset",slbind_textdomain_codeset, SLANG_VOID_TYPE),
   MAKE_INTRINSIC_0((char*)"dgettext",sldgettext, SLANG_VOID_TYPE),
   MAKE_INTRINSIC_0((char*)"textdomain",sltextdomain, SLANG_VOID_TYPE),
   MAKE_INTRINSIC_0((char*)"ngettext",slngettext, SLANG_VOID_TYPE),
   MAKE_INTRINSIC_0((char*)"dcgettext",sldcgettext, SLANG_VOID_TYPE),
   MAKE_INTRINSIC_IS("setlocale",slsetlocale, SLANG_VOID_TYPE),
   SLANG_END_INTRIN_FUN_TABLE
};

/*}}}*/
/*{{{ constant table */

static SLang_IConstant_Type gettext_IConsts [] =
{
   MAKE_ICONSTANT("LC_ALL", LC_ALL),
   MAKE_ICONSTANT("LC_COLLATE", LC_COLLATE),
   MAKE_ICONSTANT("LC_CTYPE", LC_CTYPE),
   MAKE_ICONSTANT("LC_MESSAGES", LC_MESSAGES),
   MAKE_ICONSTANT("LC_MONETARY", LC_MONETARY),
   MAKE_ICONSTANT("LC_NUMERIC", LC_NUMERIC),
   MAKE_ICONSTANT("LC_TIME", LC_TIME),
   SLANG_END_ICONST_TABLE
};

/*}}}*/
/*{{{ module init */


SLANG_MODULE(gettext);
int init_gettext_module_ns(char *ns_name)
{
   SLang_NameSpace_Type *ns = NULL;
   if (ns_name != NULL) {
	ns = SLns_create_namespace (ns_name);
	if (ns == NULL) return -1;
   }

   if ( -1 == SLns_add_iconstant_table(ns,gettext_IConsts,NULL) ||
	-1 == SLns_add_intrin_fun_table (ns,gettext_Funcs,(char*)"__gettext__"))
		return -1;
   
   if (-1 == SLclass_add_unary_op (SLANG_STRING_TYPE, string_unary,
				 string_unary_result))
     return -1;

   return 0;
}

/*}}}*/
