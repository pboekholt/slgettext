\function{gettext}
\synopsis{translate message}
\usage{String_Type gettext(String_Type msgid)}
\description
  The \ifun{gettext} function searches the currently selected message
  catalogs for a string which is equal to \exmp{msgid}.  If there is
  such a string available it is returned.  Otherwise the argument
  string \exmp{msgid} is returned.
\seealso{dgettext, ngettext, dngettext, textdomain, bindtextdomain}
\notes
  As a shorthand for \ifun{gettext}, you can use the \sfun{_}
  function.
\done

\function{textdomain}
\synopsis{set domain for future gettext() calls}
\usage{String_Type textdomain(String_Type domainname)}
\description
  The \ifun{textdomain} function sets the default domain, which is
  used in all future \ifun{gettext} calls, to \exmp{domainname}. The
  function returns the value which is from now on taken as the default
  domain.
  
  If the \exmp{domainname} parameter is NULL no new default domain is
  set.  Instead the currently selected default domain is returned.
\seealso{gettext, bindtextdomain}
\done

\function{bindtextdomain}
\synopsis{set directory containing message catalogs}
\usage{String_Type bindtextdomain(String_Type domainname, String_Type dirname)}
\description
  The \ifun{bindtextdomain} function can be used to specify the
  directory which contains the message catalogs for domain
  \exmp{domainname} for the different languages.  The function returns
  the name of the selected directory name.
  
  If the \exmp{dirname} parameter is NULL \ifun{bindtextdomain}
  returns the currently selected directory for the domain with the
  name \exmp{domainname}.
\seealso{gettext, textdomain, bindtextdomain_codeset}
\done

\function{bind_textdomain_codeset}
\synopsis{set encoding of message translations}
\usage{String_Type bind_textdomain_codeset(String_Type domainname, String_Type codeset)}
\description
  The \ifun{bind_textdomain_codeset} function can be used to specify
  the output character set for message catalogs for domain
  \exmp{domainname}. The \exmp{codeset} argument must be a valid
  codeset name which can be used for the \ifun{iconv_open} function,
  or NULL.

  If the \exmp{codeset} parameter is NULL, \ifun{bind_textdomain_codeset}
  returns the currently selected codeset for the domain with the name
  \exmp{domainname}. It returns NULL if no codeset has yet been
  selected.
  
  The function returns the name of the selected codeset.
\seealso{gettext, textdomain, bindtextdomain}
\done

\function{dgettext}
\synopsis{translate message}
\usage{String_Type dgettext(String_Type domainname, String_Type msgid)}
\description
  The \ifun{dgettext} functions acts just like the \ifun{gettext}
  function.  It only takes an additional first argument
  \exmp{domainname} which guides the selection of the message catalogs
  which are searched for the translation.
\seealso{gettext, dngettext}
\done

\function{ngettext}
\synopsis{translate message and choose plural form}
\usage{String_Type ngettext(String_Type msgid1, String_Type msgid2, Int_Type n)}
\description
  The \ifun{ngettext} function is similar to the \ifun{gettext}
  function as it finds the message catalogs in the same way.  But it
  takes two extra arguments.  The \exmp{msgid1} parameter must contain
  the singular form of the string to be converted.  It is also used as
  the key for the search in the catalog.  The \exmp{msgid2} parameter
  is the plural form.  The parameter \exmp{n} is used to determine the
  plural form.  If no message catalog is found \exmp{msgid1} is
  returned if \exmp{n} == 1, otherwise \exmp{msgid2}.
\seealso{gettext, dngettext}
\done

\function{dngettext}
\synopsis{translate message and choose plural form}
\usage{String_Type dngettext(String_Type domain, String_Type msgid1, String_Type msgid2, Int_Type n)}
\description
  The \ifun{dngettext} function is similar to the \ifun{dgettext}
  function in the way the message catalog is selected.  The difference
  is that it takes two extra parameter to provide the correct plural
  form.  These two parameters are handled in the same way
  \ifun{ngettext} handles them.
\seealso{gettext, ngettext, dgettext}
\done

\function{setlocale}
\synopsis{set the locale}
\usage{setlocale(Int_Type category, String_Type locale)}
\description
  The function \ifun{setlocale} sets the current locale for category
  \exmp{category} to \exmp{locale}.
  For \var{category}, the following constants are defined:
#v+
    LC_COLLATE  : This category applies to collation of strings
    LC_CTYPE    : affects character handling (e.g. which 8bit characters
                  are regarded as upper/lower case)
    LC_MONETARY : This category applies to formatting monetary values                   
    LC_TIME     : affects the formatting of dates (12-hour vs. 24-hour
                  clock, language of month names etc.)
    LC_MESSAGES : This category applies to selecting the language used in the user
                  interface for message translation
#v-
  The \var{locale} can be any locale supported by your system.  If
  it is an empty string, the locale to use will be taken from the
  environment.  The function will return the name of the locale that was
  actually selected.
\seealso{gettext}
\notes
  This describes the gettext module's implementation of
  \ifun{setlocale}, which should be compatible with slrn's
  implementation.
  
  In most \slang applications the locale will already be set to the
  empty string by the \cfun{SLutf8_enable} function.

  With the exception of \ifun{strftime}, most builtin \slang functions
  are not locale aware.
  
  The LC_ALL and LC_NUMERIC constants are still present for backward
  compatibility, however LC_NUMERIC does nothing and LC_ALL sets
  LC_MESSAGES.  Changing the LC_NUMERIC locale would break \slang's
  floating point support.
\done
